# encoding:utf-8
# We enable the new python 3 print syntax
from __future__ import print_function
import os

SourceFolder = ''
class PLCApplication:
    deviceName = ""
    application = None
    mciapplication = None
    
    def __init__(self, deviceName, application, mciApp):
        self.deviceName = deviceName
        self.application = application
        self.mciApp = mciApp

# Prints out all devices in the currently open project.
def findApplications(project):
    appslist = []
    for obj in project.get_children():
        if obj.is_device:
            name = obj.get_name()
            mciApp = None
            applications = obj.find('Plc Logic', recursive = False)
            
            mcis = obj.find('MCi210', recursive = True)
            if len(mcis) > 0:
                mciApp = mcis[0].find('Plc Logic', recursive = False)[0]
            
            if len(applications) > 0:
                app = PLCApplication(name,applications[0], mciApp)
                appslist.append(app)
           
                
    return appslist

def createAppFolder(basefolder, appname):
    if not os.path.isdir("{}{}".format(basefolder,appname)):
        os.mkdir("{}{}".format(basefolder,appname))

def getFolderPath (basefolder, obj, appname) :
    parent = obj.parent
    if not parent.is_folder :
        # this object is in the root folder
        return "{}{}\\".format(basefolder,appname)
    else :
        # this element is in a folder
        newbase = getFolderPath(basefolder, parent, appname)
        if newbase.endswith("Mci\\") and appname == "Mci":
            return "{}\\{}\\".format(newbase,parent.get_name()) 
        else:    
            return "{}{}\\{}\\".format(newbase,appname,parent.get_name()) 

def createFolder (basefolder, folderObj):
    if folderObj.is_folder:
        parent = folderObj.parent
        if not parent.is_folder :
            # this is a root folder
            if os.path.isdir("{}{}".format(basefolder,folderObj.get_name())):
                ## folder exists
                return "{}{}\\".format(basefolder,folderObj.get_name())                 
            else:
                #create folder in base path and return
                os.mkdir("{}{}".format(basefolder,folderObj.get_name()))
                return "{}{}\\".format(basefolder,folderObj.get_name()) 
        else:
            #is a subfolder so go back one level
            newBase = createFolder(basefolder,parent)
            if os.path.isdir("{}{}".format(newBase,folderObj.get_name())):
                ## folder exists
                return "{}{}\\".format(newBase,folderObj.get_name())                 
            else:
                #create folder in base path and return
                os.mkdir("{}{}".format(newBase,folderObj.get_name()))
                return "{}{}\\".format(newBase,folderObj.get_name()) 

def getUDTandPOU(appname,ismci,plcLogic):
    application = plcLogic.get_children()
    if application[0].is_application:
        for obj in application[0].get_children(True):
            if obj.is_folder:
                if ismci:
                    createFolder("{}\\{}\\{}\\".format(SourceFolder,appname, "Mci"), obj)
                else:
                    createFolder("{}\\{}\\".format(SourceFolder,appname), obj)
            if obj.has_textual_declaration :
                if ismci:
                    filepath = "{}{}.xml".format(getFolderPath("{}\\{}\\".format(SourceFolder,appname),obj,"Mci"),obj.get_name())
                else:
                    filepath = "{}{}.xml".format(getFolderPath(SourceFolder,obj,appname),obj.get_name())
                obj.export_xml(path=filepath, export_folder_structure=True)
    

print("--- Exporting all program objects in project: ---")
path = os.path.dirname(projects.primary.path)
project = os.path.splitext(os.path.basename(projects.primary.path))[0]
projectparts = SourceFolder = "{}\\{}\\".format(path,project)

if not os.path.isdir(SourceFolder):
        os.mkdir(SourceFolder)

res = findApplications(projects.primary)

if res is not None:
    for app in res:
        if app.application is not None:
            createAppFolder(SourceFolder,app.deviceName)
            getUDTandPOU(app.deviceName, False, app.application)
        if app.mciApp is not None :
            createAppFolder("{}\\{}\\".format(SourceFolder, app.deviceName),"Mci")
            getUDTandPOU(app.deviceName, True, app.mciApp)
        
print("--- Script finished. ---")


#           print("got application")
#        for obj1 in obj.get_children():
#            if obj1.get_name(False) == "Plc Logic":
#                print("found PLC Logic")
#        name = obj.get_name(False)
#        deviceid = obj.get_device_identification()            
#        #print(name)
#        #print(deviceid.type)